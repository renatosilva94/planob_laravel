<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Páginas*/

Route::get('pagina.inicial.sistema', 'ViewController@paginaInicialSistema')->name('pagina.inicial.sistema');
Route::get('pagina.gerencia.produtos', 'ViewController@paginaGerenciaProdutos')->name('pagina.gerencia.produtos');
Route::get('pagina.login', 'ViewController@paginaLogin')->name('pagina.login');
Route::get('pagina.cadastro', 'ViewController@paginaCadastro')->name('pagina.cadastro');
Route::get('pagina.gerencia.tickets', 'ViewController@paginaGerenciaTickets')->name('pagina.gerencia.tickets');
Route::get('pagina.gerencia.clientes', 'ViewController@paginaGerenciaClientes')->name('pagina.gerencia.clientes');
Route::get('pagina.gerencia.funcionarios', 'ViewController@paginaGerenciaFuncionarios')->name('pagina.gerencia.funcionarios');


/*Produtos*/
Route::post('salvar.produto', 'ProdutoController@salvarProduto')->name('salvar.produto');
Route::post('salvar.produto.editado/{id}', 'ProdutoController@salvarProdutoEditado')->name('salvar.produto.editado');
Route::delete('produto.deletar/{id}', 'ProdutoController@deletaProduto')->name('produto.deletar');

/*Login*/
Route::post('login.efetuar', 'UsuarioController@efetuarLogin')->name('login.efetuar');

/*Logout*/
Route::get('efetuar.logout', 'UsuarioController@efetuarLogout')->name('efetuar.logout');

/*Salvar Usuario*/
Route::post('cadastro.efetuar', 'UsuarioController@efetuarCadastro')->name('cadastro.efetuar');

/*Tickets*/
Route::post('salvar.ticket', 'TicketController@salvarTicket')->name('salvar.ticket');
Route::post('salvar.ticket.solucionado/{id}', 'TicketController@salvarTicketSolucionado')->name('salvar.ticket.solucionado');
Route::delete('ticket.deletar/{id}', 'TicketController@deletaTicket')->name('ticket.deletar');

/*Clientes*/
Route::post('salvar.cliente', 'ClienteController@salvarCliente')->name('salvar.cliente');
Route::post('salvar.cliente.editado/{id}', 'ClienteController@salvarClienteEditado')->name('salvar.cliente.editado');
Route::delete('cliente.deletar/{id}', 'ClienteController@deletaCliente')->name('cliente.deletar');

/*Funcionários*/
Route::post('salvar.funcionario', 'FuncionarioController@salvarFuncionario')->name('salvar.funcionario');
Route::post('salvar.funcionario.editado/{id}', 'FuncionarioController@salvarFuncionarioEditado')->name('salvar.funcionario.editado');
Route::delete('funcionario.deletar/{id}', 'FuncionarioController@deletaFuncionario')->name('funcionario.deletar');

