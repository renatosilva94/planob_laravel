@extends('dashboard')

@section('conteudo')


    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"> Tickets</i> </h3>
        <div class="row mt">
          <div class="col-lg-12">







          <div class="modal fade" id="modalNovoTicket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel"><b>Novo Ticket</b></h3>
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('salvar.ticket')}}">

                    {{ csrf_field() }}

                    <div class="form-group">
            <label for="titulo">Título:</label>
            <input type="text" class="form-control" id="titulo"
                   name="titulo"
                   required>
        </div>

        

        <div class="form-group">
            <label for="problema">Descreva O Problema:</label>
            <input type="text" class="form-control" id="problema"
                   name="problema" 
                   required>
        </div>


        <div class="form-group">
                                                    <label for="cliente_id">Cliente:</label>
                                                    <select class="form-control" id="cliente_id" name="cliente_id">
                                                        <option></option>
                                                        @foreach($clientes as $cliente)
                                                        <option value="{{$cliente->id}}" name="cliente_id">{{$cliente->nome}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>



            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Salvar Ticket</button>

                </form>

            </div>
        </div>
    </div>
    </div>
</div>







        

            <!-- MAIN CONTENT-->
            <div class="main-content">

            
                
                <div class="section__content section__content--p30">


              




    


        <button type="button" class="btn btn-success" data-toggle="modal" 
        data-target="#modalNovoTicket">Novo Ticket </button>



                <a href="{{route('pagina.gerencia.tickets')}}" class="btn btn-primary" 
       role="button">Ver Todos</a>
    </div>    


<br>
<br>

                    <div class="container-fluid">

                    
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-wrapper table--no-card m-b-30">

                                @if (count($tickets)==0)
    <div class="alert alert-danger">
        Não há tickets com os filtros informados...
    </div>
    @endif
    
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                        <tr>
                <th>Código</th>
                <th>Título</th>
                <th>Descrição do Problema</th>
                <th>Data de Abertura</th>
                <th>Data de Termino</th>
                <th>Foi Resolvido?</th>
                <th>Solução</th>
                <th>Cliente</th>
                
                

                <th>Ações</th>
            </tr>
                                        
                                        </thead>
                                        <tbody>

                                        @foreach($tickets as $ticket)


                                         <!-- Modal Editar Produto -->

                                         <style>
                                             
                                         .modal-backdrop {
    /* bug fix - no overlay */    
    display: none;    

}</style>

                    <div class="modal fade" id="modalSolucionarTicket{{$ticket->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title" id="exampleModalLabel"><b>Solucionar Ticket</b></h3>
                                </div>
                                <div class="modal-body">
                                    <form method="post" action="{{route('salvar.ticket.solucionado', $ticket->id)}}">

                                   {{ csrf_field() }}

                                         

 <div class="form-group">
            <label for="solucao">Solução:</label>
            <input type="text" class="form-control" id="solucao"
                   name="solucao"
                   required>
        </div>


<button type="submit" class="btn btn-primary">Encerrar Ticket Solucionado</button>        
<button type="reset" class="btn btn-warning">Limpar</button>        
</form>    

                                </div>
                            </div>
                        </div>
                    </div>




                    <!-- Fim da Modal Editar Produtos -->
























            <tr>
                <td style="text-align: center">{{$ticket->id}}</td>
                <td>{{$ticket->titulo}}</td>
                <td>{{$ticket->problema}}</td>
                <td>{{date('d/m/Y', strtotime($ticket->data_abertura))}}</td>
                <td>{{date('d/m/Y', strtotime($ticket->data_termino))}}</td>

                @if($ticket->isResolvido == 0)
                <td>Não</td>
                @else 
                <td>Sim</td>
                @endif
                








                

                <td>{{$ticket->solucao}}</td>
                <td>{{$ticket->clientes->nome}}</td>



                <td>

                @if($ticket->isResolvido == 0)
                    <a href="#" data-toggle="modal" data-target="#modalSolucionarTicket{{$ticket->id}}"
                       class="btn btn-warning btn-sm" 
                       role="button">Solucionar</a> &nbsp;&nbsp;
                       @else

                       @endif


                       
                    <form style="display: inline-block"
                          method="post"
                          action="{{route('ticket.deletar', $ticket->id)}}"
                          onsubmit="return confirm('Confirma Exclusão do Ticket?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-danger btn-sm"> Excluir </button>
                    </form> &nbsp;&nbsp;

                </td>
                @endforeach
            </tr>
                                            
                                        
                                        </tbody>
                                    </table>

                                    <br>

                                    <h4> {{ $tickets->links() }} </h4>

                                </div>
                        </div>
                </div>
            </div>




















			
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
   @endsection