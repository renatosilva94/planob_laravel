@extends('dashboard')

@section('conteudo')


    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"> Produtos</i> </h3>
        <div class="row mt">
          <div class="col-lg-12">







          <div class="modal fade" id="modalNovoProduto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel"><b>Novo Produto</b></h3>
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('salvar.produto')}}" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <div class="form-group">
            <label for="nome">Nome do Produto:</label>
            <input type="text" class="form-control" id="nome"
                   name="nome"
                   required>
        </div>

        
                
        <div class="form-group">
            <label for="quantidade">Quantidade:</label>
            <input type="number" class="form-control" id="quantidade"
                   name="quantidade" 
                   required>
        </div>
        

        <div class="form-group">
            <label for="cod_barras">Código de Barras:</label>
            <input type="number" class="form-control" id="cod_barras"
                   name="cod_barras" 
                   required>
        </div>

        <div class="form-group">
            <label for="preco_compra">Preço de Compra:</label>
            <input type="number" step=0.01 class="form-control" id="preco_compra"
                   name="preco_compra" 
                   required>
        </div>

        <div class="form-group">
            <label for="preco_venda">Preço de Venda:</label>
            <input type="number" step=0.01 class="form-control" id="preco_venda"
                   name="preco_venda" 
                   required>
        </div>



                    <div class="form-group">
                        <label for="imagem_produto"> Imagem do Produto: </label>
                        <input type="file" id="imagem_produto" name="imagem_produto"
                               onchange="previewFile()"
                               class="form-control">
                    </div>


                    <div class="col-sm-6">

                        {!!"<img src='imagens_produtos/sem_foto.png' id='imagem_produto_preview' height='150px' width='150px' alt='Foto do Produto' class='img-circle'>"!!}

                    </div>

                    <script>
                        function previewFile() {
                            var preview = document.getElementById('imagem_produto_preview');
                            var file = document.getElementById('imagem_produto').files[0];
                            var reader = new FileReader();

                            reader.onloadend = function () {
                                preview.src = reader.result;
                            };

                            if (file) {
                                reader.readAsDataURL(file);
                            } else {
                                preview.src = "";
                            }
                        }

                    </script>



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Salvar Produto</button>

                </form>

            </div>
        </div>
    </div>
</div>








        

            <!-- MAIN CONTENT-->
            <div class="main-content">

            
                
                <div class="section__content section__content--p30">


              




    


        <button type="button" class="btn btn-success" data-toggle="modal" 
        data-target="#modalNovoProduto">Novo Produto </button>



                <a href="{{route('pagina.gerencia.produtos')}}" class="btn btn-primary" 
       role="button">Ver Todos</a>
    </div>    


<br>
<br>

                    <div class="container-fluid">

                    
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-wrapper table--no-card m-b-30">

                                @if (count($produtos)==0)
    <div class="alert alert-danger">
        Não há produtos com os filtros informados...
    </div>
    @endif
    
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                        <tr>
                <th>Código</th>
                <th>Nome do Produto</th>
                <th>Quantidade</th>
                <th>Código de Barras</th>
                <th>Preço de Compra</th>
                <th>Preço de Venda</th>
                
                

                <th>Ações</th>
            </tr>
                                        
                                        </thead>
                                        <tbody>

                                        @foreach($produtos as $produto)


                                         <!-- Modal Editar Produto -->

                                         <style>
                                             
                                         .modal-backdrop {
    /* bug fix - no overlay */    
    display: none;    

}</style>

                    <div class="modal fade" id="modalEditarProduto{{$produto->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title" id="exampleModalLabel"><b>Editar Produto</b></h3>
                                </div>
                                <div class="modal-body">
                                    <form method="post" action="{{route('salvar.produto.editado', $produto->id)}}" enctype="multipart/form-data">

                                   {{ csrf_field() }}

                                          <div class="form-group">
                                        <label for="nome">Nome do Produto:</label>
                                        <input type="text" class="form-control" id="nome"
                                              name="nome" 
                                              value="{{$produto->nome or old('nome')}}"
                                              required>
                                    </div>


        
<div class="form-group">
    <label for="quantidade">Quantidade:</label>
    <input type="number" class="form-control" id="quantidade"
           name="quantidade" 
           value="{{$produto->quantidade or old('quantidade')}}"
           required>
</div>


<div class="form-group">
            <label for="cod_barras">Código de Barras:</label>
            <input type="number" class="form-control" id="cod_barras"
                   name="cod_barras"           
                    value="{{$produto->cod_barras or old('cod_barras')}}"

                   required>
        </div>

        <div class="form-group">
            <label for="preco_compra">Preço de Compra:</label>
            <input type="number" step=0.01 class="form-control" id="preco_compra"
                   name="preco_compra" 
                   value="{{$produto->preco_compra or old('preco_compra')}}"

                   required>
        </div>

        <div class="form-group">
            <label for="preco_venda">Preço de Venda:</label>
            <input type="number" step=0.01 class="form-control" id="preco_venda"
                   name="preco_venda" 
                   value="{{$produto->preco_venda or old('preco_venda')}}"

                   required>
        </div>


<div class="form-group">
                                            <label for="imagem_produto"> Imagem do Produto: </label>
                                            <input type="file" id="imagem_produto_editar" name="imagem_produto"
                                                   class="form-control">
                                                   <!-- onChange="previewFileEditar()" -->
                                        </div>


                                        <div class="col-sm-6">

                                            @php
                                            if(file_exists(public_path('imagens_produtos/'.$produto->id.'.png'))){
                                            $imagem_produto = '../imagens_produtos/'.$produto->id.'.png';
                                            } else {
                                            $imagem_produto = '../imagens_produtos/sem_foto.png';
                                            }
                                            @endphp

                                            {!!"<img src=$imagem_produto id='imagem_produto_preview_editar' height='150px' width='150px' alt='Foto do Produto' class='img-cirle'>"!!}

                                        </div>



                                        <script>
                                            window.onload = () => {

                                                $('input[type="file"]').on('change', e => {
                                                    const activeModal = $('.modal.fade.in');
                                                    const preview = activeModal.find('#imagem_produto_preview_editar');
                                                    const file = e.target.files[0];
                                                    const reader = new FileReader();
                                                    
                                                    reader.onload = function () {
                                                        preview.attr('src', reader.result);
                                                    };
                                                    
                                                    if (file) {
                                                        reader.readAsDataURL(file);
                                                    } else {
                                                        preview.src = "";
                                                    }
                                                });
                                                
                                               
                                            }

                                        </script>


<button type="submit" class="btn btn-primary">Salvar Produto</button>        
<button type="reset" class="btn btn-warning">Limpar</button>        
</form>    

                                </div>
                            </div>
                        </div>
                    </div>




                    <!-- Fim da Modal Editar Produtos -->
























            <tr>
                <td style="text-align: center">{{$produto->id}}</td>
                <td>{{$produto->nome}}</td>
                <td>{{$produto->quantidade}}</td>
                <td>{{$produto->cod_barras}}</td>
                <td>{{$produto->preco_compra}}</td>
                <td>{{$produto->preco_venda}}</td>
                
                
                <td>
                    <a href="#" data-toggle="modal" data-target="#modalEditarProduto{{$produto->id}}"
                       class="btn btn-warning btn-sm" 
                       role="button">Editar</a> &nbsp;&nbsp;


                       
                    <form style="display: inline-block"
                          method="post"
                          action="{{route('produto.deletar', $produto->id)}}"
                          onsubmit="return confirm('Confirma Exclusão do Produto?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-danger btn-sm"> Excluir </button>
                    </form> &nbsp;&nbsp;

                </td>
                @endforeach
            </tr>
                                            
                                        
                                        </tbody>
                                    </table>

                                    <br>

                                    <h4> {{ $produtos->links() }} </h4>

                                </div>
                        </div>
                </div>
            </div>




















			
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
   @endsection