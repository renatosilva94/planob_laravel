@extends('dashboard')

@section('conteudo')


    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"> Funcionarios</i> </h3>
        <div class="row mt">
          <div class="col-lg-12">







          <div class="modal fade" id="modalNovoFuncionario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel"><b>Novo Funcionario</b></h3>
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('salvar.funcionario')}}" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <div class="form-group">
            <label for="nome">Nome:</label>
            <input type="text" class="form-control" id="nome"
                   name="nome"
                   required>
        </div>

        
                
        <div class="form-group">
            <label for="sobrenome">Sobrenome:</label>
            <input type="text" class="form-control" id="sobrenome"
                   name="sobrenome" 
                   required>
        </div>
        

        <div class="form-group">
            <label for="email">E-Mail:</label>
            <input type="text" class="form-control" id="email"
                   name="email" 
                   required>
        </div>
        </div>

                <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Salvar Funcionario</button>

                </form>

            </div>
        </div>
    </div>
</div>








        

            <!-- MAIN CONTENT-->
            <div class="main-content">

            
                
                <div class="section__content section__content--p30">


              




    


        <button type="button" class="btn btn-success" data-toggle="modal" 
        data-target="#modalNovoFuncionario">Novo Funcionario </button>



                <a href="{{route('pagina.gerencia.funcionarios')}}" class="btn btn-primary" 
       role="button">Ver Todos</a>
    </div>    


<br>
<br>

                    <div class="container-fluid">

                    
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-wrapper table--no-card m-b-30">

                                @if (count($funcionarios)==0)
    <div class="alert alert-danger">
        Não há funcionarios com os filtros informados...
    </div>
    @endif
    
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                        <tr>
                <th>Código</th>
                <th>Nome</th>
                <th>Sobrenome</th>
                <th>E-Mail</th>
                
                

                <th>Ações</th>
            </tr>
                                        
                                        </thead>
                                        <tbody>

                                        @foreach($funcionarios as $funcionario)


                                         <!-- Modal Editar Produto -->

                                         <style>
                                             
                                         .modal-backdrop {
    /* bug fix - no overlay */    
    display: none;    

}</style>

                    <div class="modal fade" id="modalEditarFuncionario{{$funcionario->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title" id="exampleModalLabel"><b>Editar Funcionario</b></h3>
                                </div>
                                <div class="modal-body">
                                    <form method="post" action="{{route('salvar.funcionario.editado', $funcionario->id)}}" enctype="multipart/form-data">

                                   {{ csrf_field() }}

                                          <div class="form-group">
                                        <label for="nome">Nome:</label>
                                        <input type="text" class="form-control" id="nome"
                                              name="nome" 
                                              value="{{$funcionario->nome or old('nome')}}"
                                              required>
                                    </div>


        
<div class="form-group">
    <label for="sobrenome">Sobrenome:</label>
    <input type="text" class="form-control" id="sobrenome"
           name="sobrenome" 
           value="{{$funcionario->sobrenome or old('sobrenome')}}"
           required>
</div>


<div class="form-group">
            <label for="email">E-Mail:</label>
            <input type="text" class="form-control" id="email"
                   name="email"           
                    value="{{$funcionario->email or old('email')}}"

                   required>
        </div>



<button type="submit" class="btn btn-primary">Salvar Funcionario</button>        
<button type="reset" class="btn btn-warning">Limpar</button>        
</form>    

                                </div>
                            </div>
                        </div>
                    </div>




                    <!-- Fim da Modal Editar Funcionario -->
























            <tr>
                <td style="text-align: center">{{$funcionario->id}}</td>
                <td>{{$funcionario->nome}}</td>
                <td>{{$funcionario->sobrenome}}</td>
                <td>{{$funcionario->email}}</td>
                
                
                <td>
                    <a href="#" data-toggle="modal" data-target="#modalEditarFuncionario{{$funcionario->id}}"
                       class="btn btn-warning btn-sm" 
                       role="button">Editar</a> &nbsp;&nbsp;


                       
                    <form style="display: inline-block"
                          method="post"
                          action="{{route('funcionario.deletar', $funcionario->id)}}"
                          onsubmit="return confirm('Confirma Exclusão do Funcionário?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-danger btn-sm"> Excluir </button>
                    </form> &nbsp;&nbsp;

                </td>
                @endforeach
            </tr>
                                            
                                        
                                        </tbody>
                                    </table>

                                    <br>

                                    <h4> {{ $funcionarios->links() }} </h4>

                                </div>
                        </div>
                </div>
            </div>




















			
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
   @endsection