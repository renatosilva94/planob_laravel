@extends('dashboard')

@section('conteudo')


    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"> Clientes</i> </h3>
        <div class="row mt">
          <div class="col-lg-12">







          <div class="modal fade" id="modalNovoCliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel"><b>Novo Cliente</b></h3>
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('salvar.cliente')}}">
<br>



                    {{ csrf_field() }}


                    <!-- <div class="form-group">
            <label for="cnpj">CNPJ:</label>
            <input type="text" class="form-control" id="cnpj"
                   name="cnpj"
                   >
        </div>


        <div class="form-group">
            <label for="cpf">CPF:</label>
            <input type="text" class="form-control" id="cpf"
                   name="cpf"
                   >
        </div>
        

        <div class="form-group ">
            <label for="tipo">Tipo:</label>
            <input type="text" class="form-control" id="tipo"
                   name="tipo"
                   >
        </div> -->

        <div class="form-group">
            <label for="nome">Nome:</label>
            <input type="text" class="form-control" id="nome"
                   name="nome"
                   >
        </div>

    

        <div class="form-group">
            <label for="logradouro">Logradouro:</label>
            <input type="text" class="form-control" id="logradouro"
                   name="logradouro"
                   >
        </div>

        <div class="form-group">
            <label for="numero">Número:</label>
            <input type="text" class="form-control" id="numero"
                   name="numero"
                   >
        </div>

        <div class="form-group">
            <label for="bairro">Bairro:</label>
            <input type="text" class="form-control" id="bairro"
                   name="bairro"
                   >
        </div>

        <div class="form-group">
            <label for="municipio">Municipio:</label>
            <input type="text" class="form-control" id="municipio"
                   name="municipio"
                   >
        </div>
        
        <div class="form-group">
            <label for="cep">CEP:</label>
            <input type="text" class="form-control" id="cep"
                   name="cep"
                   >
        </div>

        <div class="form-group">
            <label for="uf">UF:</label>
            <input type="text" class="form-control" id="uf"
                   name="uf"
                   >
        </div>

        <div class="form-group">
            <label for="email">E-Mail:</label>
            <input type="text" class="form-control" id="email"
                   name="email"
                   >
        </div>

        
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Salvar Cliente</button>

                </form>

            </div>
        </div>
    </div>
    </div>
</div>







        

            <!-- MAIN CONTENT-->
            <div class="main-content">

            
                
                <div class="section__content section__content--p30">


              




    


        <button type="button" class="btn btn-success" data-toggle="modal" 
        data-target="#modalNovoCliente">Novo Cliente </button>



                <a href="{{route('pagina.gerencia.clientes')}}" class="btn btn-primary" 
       role="button">Ver Todos</a>
    </div>    


<br>
<br>

                    <div class="container-fluid">

                    
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-wrapper table--no-card m-b-30">

                                @if (count($clientes)==0)
    <div class="alert alert-danger">
        Não há clientes com os filtros informados...
    </div>
    @endif
    
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                        <tr>
                <th>Código</th>
                <th>Nome</th>
                <th>Tipo de Cliente</th>
                
                

                <th>Ações</th>
            </tr>
                                        
                                        </thead>
                                        <tbody>

                                        @foreach($clientes as $cliente)


                                         <!-- Modal Editar Produto -->

                                         <style>
                                             
                                         .modal-backdrop {
    /* bug fix - no overlay */    
    display: none;    

}</style>

                    <div class="modal fade" id="modalEditarCliente{{$cliente->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title" id="exampleModalLabel"><b>Editar Cliente</b></h3>
                                </div>
                                <div class="modal-body">
                                    <form method="post" action="{{route('salvar.cliente.editado', $cliente->id)}}">

                                   {{ csrf_field() }}

                                         

 
 


<button type="submit" class="btn btn-primary">Salvar Cliente Editado</button>        
<button type="reset" class="btn btn-warning">Limpar</button>        
</form>    

                                </div>
                            </div>
                        </div>
                    </div>




                    <!-- Fim da Modal Editar Produtos -->
























            <tr>
                <td style="text-align: center">{{$cliente->id}}</td>
                <td>{{$cliente->nome}}</td>
                

                @if($cliente->isEmpresa == 0)
                <td>Pessoa Física</td>
                @else 
                <td>Pessoa Jurídica</td>
                @endif
                








                




                <td>

               
                    <a href="#" data-toggle="modal" data-target="#modalEditarCliente{{$cliente->id}}"
                       class="btn btn-warning btn-sm" 
                       role="button">Editar</a> &nbsp;&nbsp;
                       


                       
                    <form style="display: inline-block"
                          method="post"
                          action="{{route('cliente.deletar', $cliente->id)}}"
                          onsubmit="return confirm('Confirma Exclusão do Cliente?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-danger btn-sm"> Excluir </button>
                    </form> &nbsp;&nbsp;

                </td>
                @endforeach
            </tr>
                                            
                                        
                                        </tbody>
                                    </table>

                                    <br>

                                    <h4> {{ $clientes->links() }} </h4>

                                </div>
                        </div>
                </div>
            </div>




















			
          </div>
        </div>
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
   @endsection