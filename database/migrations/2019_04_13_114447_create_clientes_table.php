<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cnpj')->nullable();
            $table->string('cpf')->nullable();
            $table->string('tipo')->nullable();
            $table->string('nome');
            $table->string('fantasia')->nullable();
            $table->string('logradouro');
            $table->string('numero');
            $table->string('bairro');
            $table->string('municipio');
            $table->string('cep');
            $table->string('uf');
            $table->string('email');          
            $table->boolean('isEmpresa');
            $table->timestamps();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
