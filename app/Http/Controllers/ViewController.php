<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produtos;
use App\Usuarios;
use App\Tickets;
use App\Clientes;
use App\Funcionarios;
use Auth;




class ViewController extends Controller
{
   
    public function paginaInicialSistema(){

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }

        $usuario_autenticado_id = Auth::guard()->user()->id;
        $usuario_autenticado_nome = Auth::guard()->user()->nome;



        return view('pagina_inicial_sistema', compact('usuario_autenticado_id','usuario_autenticado_nome'));

    }

    public function paginaLogin(){

        return view('pagina_login');

    }

    public function paginaCadastro(){

        return view('pagina_cadastro');

    }

    public function paginaGerenciaProdutos(){

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }

        

        $usuario_autenticado_id = Auth::guard()->user()->id;
        $usuario_autenticado_nome = Auth::guard()->user()->nome;



        $produtos = Produtos::where('usuario_id', $usuario_autenticado_id)->paginate(10);

        return view('pagina_gerencia_produtos', compact('produtos','usuario_autenticado_id','usuario_autenticado_nome'));

    }


    public function paginaGerenciaTickets(){

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }

        $usuario_autenticado_id = Auth::guard()->user()->id;
        $usuario_autenticado_nome = Auth::guard()->user()->nome;


        $clientes = Clientes::where('usuario_id', $usuario_autenticado_id)->get();
        $tickets = Tickets::where('usuario_id', $usuario_autenticado_id)->paginate(10);

        return view('pagina_gerencia_tickets', compact('clientes','tickets','usuario_autenticado_id','usuario_autenticado_nome'));

    }

    public function paginaGerenciaClientes(){

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }

        $usuario_autenticado_id = Auth::guard()->user()->id;
        $usuario_autenticado_nome = Auth::guard()->user()->nome;



        $clientes = Clientes::where('usuario_id', $usuario_autenticado_id)->paginate(10);

        return view('pagina_gerencia_clientes', compact('clientes','usuario_autenticado_id','usuario_autenticado_nome'));

    }


    public function paginaGerenciaFuncionarios(){

        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }

        $usuario_autenticado_id = Auth::guard()->user()->id;
        $usuario_autenticado_nome = Auth::guard()->user()->nome;



        $funcionarios = Funcionarios::where('usuario_id', $usuario_autenticado_id)->paginate(10);

        return view('pagina_gerencia_funcionarios', compact('funcionarios','usuario_autenticado_id','usuario_autenticado_nome'));

    }
    
}
