<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Funcionarios;
use Auth;

class FuncionarioController extends Controller
{
    public function salvarFuncionario(Request $request) {

        $usuario_autenticado_id = Auth::guard()->user()->id;

        $funcionarios = Funcionarios::create([
                    'nome' => $request['nome'],
                    'sobrenome' => $request['sobrenome'],
                    'email' => $request['email'],
                    'usuario_id' => $usuario_autenticado_id,


                    
        ]);

        if ($funcionarios) {

            return redirect()->route('pagina.gerencia.funcionarios');
        }
    }

    


    public function salvarFuncionarioEditado(Request $request, $id) {

        $this->validate($request, [
            'nome' => 'required',
            'sobrenome' => 'required',
            'email' => 'required',
            
        ]);

        $dados = $request->all();

        $funcionarioId = Funcionarios::find($id);

        $alt = $funcionarioId->update($dados);

        if ($alt) {

           
            return redirect()->route('pagina.gerencia.funcionarios');
        }
    }

    public function deletafuncionario($id) {
        $funcionario = Funcionarios::find($id);
        if ($funcionario->delete()) {
            return redirect()->route('pagina.gerencia.funcionarios');
        }
    }

}
