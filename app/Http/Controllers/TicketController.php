<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tickets;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Auth;



class TicketController extends Controller
{
    public function salvarTicket(Request $request) {

        $dataAtual = Carbon::today();

        $usuario_autenticado_id = Auth::guard()->user()->id;



        $tickets = Tickets::create([
                    'titulo' => $request['titulo'],
                    'problema' => $request['problema'],
                    'data_abertura' => $dataAtual->toDateString(),
                    'isResolvido' => false,
                    'cliente_id' => $request['cliente_id'],
                    'usuario_id' => $usuario_autenticado_id,


                    
        ]);

        if ($tickets) {

           

            return redirect()->route('pagina.gerencia.tickets');
        }
    }

    


    public function salvarTicketSolucionado(Request $request, $id) {

        $dataAtual = Carbon::today();


        $this->validate($request, [
            'solucao' => 'required',            
        ]);

        $dados = $request->all();

        $ticketId = Tickets::find($id);

        $alt = $ticketId->update($dados);

        if ($alt) {


            DB::table('tickets')
            ->where('id', $request['id'])
            ->update(['isResolvido' => 1]);

            DB::table('tickets')
            ->where('id', $request['id'])
            ->update(['data_termino' => $dataAtual->toDateString()]);


            return redirect()->route('pagina.gerencia.tickets');

        }
    }

    public function deletaTicket($id) {
        $ticket = Tickets::find($id);
        if ($ticket->delete()) {
            return redirect()->route('pagina.gerencia.tickets');
        }
    }

    public function listaTicketAndroid(){

        //$tickets = Tickets::all();

       $tickets = DB::table('tickets')
            ->join('clientes', 'clientes.id', '=', 'tickets.cliente_id')
            ->select('tickets.*', 'clientes.nome')
            ->get();

        return response()->json($tickets);


    }


}
