<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clientes;
use Auth;


class ClienteController extends Controller
{
    public function salvarCliente(Request $request) {


        $usuario_autenticado_id = Auth::guard()->user()->id;



        $clientes = Clientes::create([
                    'cnpj' => $request['cnpj'],
                    'cpf' => $request['cpf'],
                    'tipo' => $request['tipo'],
                    'nome' => $request['nome'],
                    'fantasia' => $request['fantasia'],
                    'logradouro' => $request['logradouro'],
                    'numero' => $request['numero'],
                    'bairro' => $request['bairro'],
                    'municipio' => $request['municipio'],
                    'cep' => $request['cep'],
                    'uf' => $request['uf'],
                    'fantasia' => $request['fantasia'],
                    'isEmpresa' => false,
                    'email' => $request['email'],
                    'usuario_id' => $usuario_autenticado_id,


                    


                    
                    
        ]);

        if ($clientes) {

          
            return redirect()->route('pagina.gerencia.clientes');
        }
    }

    


    public function salvarClienteEditado(Request $request, $id) {

      

        $dados = $request->all();

        $clienteId = Clientes::find($id);

        $alt = $clienteId->update($dados);

        if ($alt) {

            

            return redirect()->route('pagina.gerencia.clientes');
        }
    }

    public function deletaCliente($id) {
        $cliente = Clientes::find($id);
        if ($cliente->delete()) {
            return redirect()->route('pagina.gerencia.clientes');
        }
    }
}
