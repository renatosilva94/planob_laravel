<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuarios;
use Auth;
use Carbon\Carbon;
use Socialite;
use Illuminate\Support\Facades\DB;





class UsuarioController extends Controller
{
    public function redirectToProvider($provider) {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider) {
        $usuario = Socialite::driver($provider)->user();

        $authUser = $this->findOrCreateUser($usuario, $provider);
        Auth::login($authUser, true);

        // dd($authUser);
        //auth()->guard('usuarios')->attempt($authUser, true);
        return redirect('pagina.inicial');
    }

    public function findOrCreateUser($usuario, $provider) {
        $authUser = Usuarios::where('provider_id', $usuario->id)->first();
        if ($authUser) {
            return $authUser;
        }

        $dataAtual = Carbon::today();

        return Usuarios::create([
                    'nome' => $usuario->name,
                    'sobrenome' => " ",
                    'data_nascimento' => $dataAtual->toDateString(),
                    'email' => $usuario->email,
                    'provider' => $provider,
                    'provider_id' => $usuario->id,
                    'avatar' => $usuario->avatar,
        ]);
    }

    public function efetuarCadastro(Request $request) {

        $request->validate([
            'nome' => 'required|max:255',
            'sobrenome' => 'required|max:255',
            'email' => 'required|email|unique:usuarios|max:255',
            'password' => 'required|min:8|max:255',
            'data_nascimento' => 'required',
        ]);

        $usuarios = Usuarios::create([
                    'nome' => $request['nome'],
                    'sobrenome' => $request['sobrenome'],
                    'email' => $request['email'],
                    'password' => bcrypt($request['password']),
                    'data_nascimento' => $request['data_nascimento'],
        ]);

        if ($usuarios) {

            alert()->success("Cadastro Efetuado Com Sucesso !");


            return redirect()->route('pagina.login');
        }
    }

    public function efetuarLogin(Request $request) {

        $validator = validator($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {

            alert()->error("Você deve digitar E-Mail e Senha !", "Tente Novamente");



            return redirect('pagina.login');
        }

        $credentials = ['email' => $request->get('email'), 'password' => $request->get('password')];

        if (auth()->guard()->attempt($credentials)) {



            return redirect()->route('pagina.inicial.sistema');
        } else {

            alert()->error("E-Mail ou/e Senha Inválida(os) ! ", "Tente Novamente");



            return redirect('pagina.login');
        }
    }

    public function efetuarLogout() {

        auth()->guard()->logout();

        return redirect('pagina.login');
    }

    public function salvarFoto(Request $request) {

        $usuario_autenticado_id = Auth::guard('usuarios')->user()->id;
        $usuario_autenticado_nome = Auth::guard('usuarios')->user()->nome;

        $request->validate([

            'foto_perfil' => 'mimes:jpeg,bmp,png,jpg',

        ]);


        $dados = $request->all();

        if (isset($dados['foto_perfil'])) {

            $id = $usuario_autenticado_id;
            $fotoPerfilId = $id . '.jpg';
            $request->foto_perfil->move(public_path('imagens_usuarios'), $fotoPerfilId);
        }

        return redirect()->route('pagina.inicial.sistema');
    }

    public function atualizarMeuPerfil(Request $request, $id) {


        if (!auth()->guard()->user()) {
            return redirect('pagina.login');
        }




        $usuario_autenticado_id = Auth::guard()->user()->id;

        $dados = $request->all();

        $usuarioId = Usuarios::find($id);

        $alt = $usuarioId->update($dados);

        if ($alt) {


            alert()->info('Perfil Atualizado Com Sucesso.');


            return redirect()->route('pagina.inicial.sistema');
        }
    }


}
