<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produtos;
use Illuminate\Support\Facades\Input;
use Auth;


class ProdutoController extends Controller
{
    public function salvarProduto(Request $request) {

        $usuario_autenticado_id = Auth::guard()->user()->id;


        $produtos = Produtos::create([
                    'nome' => $request['nome'],
                    'quantidade' => $request['quantidade'],
                    'cod_barras' => $request['cod_barras'],
                    'preco_venda' => $request['preco_venda'],
                    'preco_compra' => $request['preco_compra'],
                    'usuario_id' => $usuario_autenticado_id,


                    
                    
        ]);

        if ($produtos) {

            if (Input::file('imagem_produto')) {

                $ultimoId = $produtos->id;
                $imagemProdutoId = $ultimoId . '.png';
                $request->imagem_produto->move(public_path('imagens_produtos/'), $imagemProdutoId);
            } else {
                
            }

            return redirect()->route('pagina.gerencia.produtos');
        }
    }

    


    public function salvarProdutoEditado(Request $request, $id) {

        $this->validate($request, [
            'nome' => 'required',
            'quantidade' => 'required',
            'cod_barras' => 'required',
            'preco_venda' => 'required',
            'preco_compra' => 'required',
            
        ]);

        $dados = $request->all();

        $produtoId = Produtos::find($id);

        $alt = $produtoId->update($dados);

        if ($alt) {

            if (Input::file('imagem_produto')) {

                $ultimoId = $id;
                $imagemProdutoId = $ultimoId . '.png';
                $request->imagem_produto->move(public_path('imagens_produtos/'), $imagemProdutoId);
            } else {
                
            }

            return redirect()->route('pagina.gerencia.produtos');
        }
    }

    public function deletaProduto($id) {
        $produto = Produtos::find($id);
        if ($produto->delete()) {
            return redirect()->route('pagina.gerencia.produtos');
        }
    }




}
