<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model {

    public $timestamps = true;
    protected $fillable = array('email','cnpj','cpf','nome','fantasia','tipo','logradouro','numero','bairro','municipio','cep','uf','isEmpresa','usuario_id');
    
    public function usuarios()
    {
        return $this->hasOne('App\Usuarios', 'id', 'usuario_id');
    }


}
