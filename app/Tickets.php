<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tickets extends Model {

    public $timestamps = true;
    protected $fillable = array('titulo', 'problema', 'data_abertura', 'isResolvido', 'data_termino','solucao','usuario_id','cliente_id');
    
    public function usuarios()
    {
        return $this->hasOne('App\Usuarios', 'id', 'usuario_id');
    }

    public function clientes()
    {
        return $this->hasOne('App\Clientes', 'id', 'cliente_id');
    }



}
