<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Usuarios extends Authenticable
{

    use Notifiable, HasApiTokens;

    public $timestamps = true;

    protected $fillable = array('provider', 'provider_id', 'nome', 'sobrenome', 'email', 'password', 'avatar', 'data_nascimento');

    protected $hidden = array('password', 'remember_token');


    

}
