<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produtos extends Model {

    public $timestamps = true;
    protected $fillable = array('nome', 'quantidade', 'cod_barras', 'preco_compra', 'preco_venda','usuario_id');
    
    public function usuarios()
    {
        return $this->hasOne('App\Usuarios', 'id', 'usuario_id');
    }

}
