<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funcionarios extends Model {

    public $timestamps = true;
    protected $fillable = array('nome', 'sobrenome', 'email','usuario_id');
    
    public function usuarios()
    {
        return $this->hasOne('App\Usuarios', 'id', 'usuario_id');
    }


}
